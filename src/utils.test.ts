import { describe, it, expect } from 'vitest'
import { resolveAnimalType, sortAnimals } from '@/utils'
import { createAnimal } from '@/test-utils'

describe('resolveAnimalType', () => {
  it('resolves type correctly', () => {
    expect(
      resolveAnimalType(
        createAnimal({ icon_uri: 'https://acnhapi.com/v1/icons/sea/1' })
      )
    ).toBe('sea')
    // [TODO] vérifier que la méthode resolveAnimalType retourne "sea" si l'icône comporte la chaîne de caractères "sea"

    // [TODO] vérifier que la méthode resolveAnimalType retourne "fish" si l'icône comporte la chaîne de caractères "fish"

    // [TODO] vérifier que la méthode resolveAnimalType retourne une erreur si l'icône ne comporte ni "sea", "fish" ou "bugs".
    // Au besoin corriger la focnction testée
    // Voir API [toThrowError](https://vitest.dev/api/#tothrowerror)
  })
})

describe('sortAnimals', () => {
  it('sorts in ascending order', () => {
    const unorderedAnimals = [
      createAnimal({ name: { 'name-EUen': 'b' } }),
      createAnimal({ name: { 'name-EUen': 'a' } }),
      createAnimal({ name: { 'name-EUen': 'c' } }),
    ]

    const orderedAnimals = sortAnimals(unorderedAnimals, true)
    expect(unorderedAnimals[0].name['name-EUen']).toBe('b')
    expect(unorderedAnimals[1].name['name-EUen']).toBe('a')
    expect(unorderedAnimals[2].name['name-EUen']).toBe('c')

    expect(orderedAnimals[0].name['name-EUen']).toBe('a')
    expect(orderedAnimals[1].name['name-EUen']).toBe('b')
    expect(orderedAnimals[2].name['name-EUen']).toBe('c')
  })

  // [TODO] rédiger un test similaire au précédent, validant que la méthode "sortAnimals" permet de trier les animaux dans un ordre décroissant
})

// [TODO] tester la méthode isAvailable en provenant de './utils'
// Vous aurez besoin de mocker l'heure et la date pour maîntriser vos cas de tests
// Voir [Mocking guide](https://vitest.dev/guide/mocking.html#dates) en particulier les API "useFakeTimers" et "useRealTimers"
