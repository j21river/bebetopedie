import SearchForm from '@/components/SearchForm.vue'
import { useStore, initialState, type MainStoreState } from '@/store'

const toggleBtnSelector = '[data-cy=search-form-toggle-button]'

describe('SearchForm [desktop]', () => {
  const store = useStore()
  beforeEach(() => {
    store.$patch(initialState() as MainStoreState)
    cy.viewport(1024, 768)
  })
  it('hides toogle button and displays the form', () => {
    cy.mount(SearchForm)
    cy.get(toggleBtnSelector).should('not.exist')
    cy.get('form').should('be.visible')
  })
  it('animalTypesSelected from the store is correctly updated', () => {
    cy.mount(SearchForm)
    cy.wrap(store).its('animalTypesSelected').should('have.length', 3)
    cy.get('[data-cy=search-form-types]').each((checkbox) => {
      cy.wrap(checkbox).should('be.checked')
    })
    cy.get('[data-cy=search-form-types]').first().uncheck()
    cy.wrap(store).its('animalTypesSelected').should('have.length', 2)
  })

  it('searchTerm from the store is correctly updated', () => {
    cy.mount(SearchForm)
    cy.wrap(store).its('searchTerm').should('have.length', 0)
    cy.get('[data-cy=search-form-term]').type('abalone')
    cy.wrap(store).its('searchTerm').should('eq', 'abalone')
  })

  it('onlyAvailableAnimals from the store is correctly updated', () => {
    cy.mount(SearchForm)
    cy.wrap(store).its('onlyAvailableAnimals').should('be.false')
    cy.contains('Only show available animals').click()
    cy.wrap(store).its('onlyAvailableAnimals').should('be.true')
  })

  // [TODO] vérifier que la propriété ascendingOrder est correctement initialisée.
  // Vérifier que le bouton comporte le label "Sort Z-A" initialement.
  // Vérifier qu'en cliquant sur le bouton,
  // le label et la propriété ascendingOrder en provenance du store sont correctement modifiées
})

describe('SearchForm [mobile]', () => {
  // [TODO] vérifier que sur mobile, le formulaire est caché initialement
  // et qu'en cliquant sur le bouton, celui-ci devient visible.
  // Vérifier que le bouton permettant d'afficer/dissimuler le formulaire
  // contient le label adéquat en fonction de la visibilité du formulaire.
})
