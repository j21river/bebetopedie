import AnimalType from './AnimalType.vue'

describe('AnimalType', () => {
  it('fish tag is rendered properly', () => {
    cy.mount(AnimalType, { props: { animalType: 'fish' } })
    cy.get('span').should('contain', 'Fish')
    cy.get('span').should('have.length', 1)
    cy.get('span').should('have.class', 'is-fish')
  })

  // [TODO] ajouter un test validant que le composant AnimalType se comporte correctement quand "bug" est passé en props

  // [TODO] ajouter un test validant que le composant AnimalType se comporte correctement quand "sea" est passé en props

  // [TODO] ajouter un test validant que le composant AnimalType n'affiche aucun tag si un type inexistant est passé en props
  // Modifier le composant au besoin pour que le test passe
})
