describe('HomeView', () => {
  it('lists animals in order', () => {
    cy.visit('/')
    cy.get('[data-cy=card-animal]').first().should('contain', 'abalone')
    cy.contains('Sort Z-A')
  })

  // [TODO] vérifier que les animaux peuvent être triés en ordre décroissant

  it('filters animals by type', () => {
    cy.visit('/')
    cy.get('[data-cy=card-animal]').first().should('contain', 'abalone')
    cy.get('[data-cy=card-animal]').first().should('contain', 'Sea creature')
    cy.get('[data-cy=search-form-types]').uncheck(['sea', 'bugs'])
    cy.get('[data-cy=card-animal]').first().should('contain', 'anchovy')
    cy.get('[data-cy=card-animal]').first().should('contain', 'Fish')
  })

  // [TODO] vérifier que la recherche par nom fonctionne en recherchant un animal spécifique au moyen de la barre de recherche

  // [TODO] vérifier qu'en cliquant sur le 1er animal de la liste, il soit possible d'accéder à la page détaillant celui-ci
})
